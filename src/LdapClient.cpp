#include "LdapClient.h"
#include <ldap.h>
#include <iostream>

namespace ldap_inj {

    const std::string BASE = "dc=example,dc=com";

    bool LdapClient::connect(const std::string& host, int port) {
        const std::string url = "ldap://" + host + ":" + std::to_string(port);

        if (int status = ldap_initialize(&m_handler, url.c_str()); status == LDAP_SUCCESS ) {
            
            const int version = LDAP_VERSION3;
            if (status = ldap_set_option(m_handler, LDAP_OPT_PROTOCOL_VERSION, &version); status == LDAP_OPT_SUCCESS) {
                
                const std::string adminUser = "cn=admin," + BASE;
                std::string password = "adminpassword";
  
                struct berval cred;
                cred.bv_val = &password[0];
                cred.bv_len = static_cast<ber_len_t>(password.length());
                if (status = ldap_sasl_bind_s(m_handler, adminUser.data(), nullptr, &cred, nullptr, nullptr, nullptr); status == LDAP_SUCCESS) {
                    return true;
                }
            }
        }
        return false;
    }

    LdapClient::~LdapClient() {
        if (m_handler != nullptr) {
            ldap_unbind_ext_s(m_handler, nullptr, nullptr);
        }
    }
    
    static std::string data(LDAP* m_handler, LDAPMessage* entry, char* attribute) {
        std::string result;
        if (struct berval** vals = ldap_get_values_len(m_handler, entry, attribute); vals != nullptr) {
            for(int i = 0; vals[i]; i++)
                result += vals[i]->bv_val;

            ldap_value_free_len(vals);
        }
        return result;
    }

    bool LdapClient::hasUser(const std::string& user, const std::string& password) {
        LDAPMessage* res;
        const std::string filter = "(cn=" + user + ")";
        std::string uid, pas;

        if (int status = ldap_search_ext_s(
            m_handler, 
            BASE.c_str(), 
            LDAP_SCOPE_SUBTREE, 
            filter.c_str(), 
            nullptr, 
            0, 
            nullptr,
            nullptr, 
            nullptr, 
            LDAP_NO_LIMIT,
            &res); status == LDAP_SUCCESS ) {

            if (LDAPMessage* entry = ldap_first_entry(m_handler, res); entry != nullptr ) {
                BerElement* ber;
                char* attribute = ldap_first_attribute(m_handler, entry, &ber);
                while(attribute) {
                    if (not strcmp(attribute, "uid")) { // if equal
                        uid = data(m_handler, entry, attribute);
                    } else if (not strcmp(attribute, "userPassword")) { // if equal
                        pas = data(m_handler, entry, attribute);
                    }

                    ldap_memfree(attribute);
                    attribute = ldap_next_attribute(m_handler, entry, ber);
                }
            }
            ldap_msgfree(res);
        }
        if (user == uid) {
            if (password.empty()) {
                return true;
            } else {
                return (pas == password);
            }
        }
        return false;
    }



}